import { defineStore } from "pinia";

export const useDataStore = defineStore("DataStore", {

    //state
    state: () =>{
        
        return {
            ENDPOINT_PATH_API: "http://localhost:8081/",
            token: null,
            members: []
            
        };
    },

    getters: {
        headersAxios: (state) => [{
            "Content-type": "application/json; charset=UTF-8",
            "Authorization": 'Bearer ' + state.token
        }]
    },

    actions: {
        setearToken(token) {
            this.token = token
        },
        addMember(member) {
            this.members.push(member)
        }
    }

})

