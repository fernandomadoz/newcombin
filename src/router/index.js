import { createRouter, createWebHistory } from 'vue-router'
import MainScreenView from '../views/MainScreenView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: MainScreenView
    }
  ]
})

export default router
