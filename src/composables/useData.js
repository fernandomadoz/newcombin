
import { storeToRefs } from 'pinia';
import { useDataStore } from '../stores/DataStore';

// Adapter Model
export const useData = () => {

    const DataStore = useDataStore();
    const {ENDPOINT_PATH_API, token, headersAxios, members} = storeToRefs(DataStore);

    const setearToken = (token) => {
        DataStore.setearToken(token)
    }

    const addMember = (member) => {
        DataStore.addMember(member)
    }

    return {
        ENDPOINT_PATH_API,
        token, 
        headersAxios,
        members,
        setearToken,
        addMember
    }
} 